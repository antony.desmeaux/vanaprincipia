To get more information on our project, have a look at https://www.vanaprincipia.com.

Feel free to contact us at contact@vanaprincipia.com (any help or advice are more than welcome!).

This project is in progress and will be continuously improved.

The goal of this software (named Vana) is to generate the most appropriate plant architectures for specific coordinates.

You will find 3 database in vanaprincipia_database (postgreSQL 14.5):
- hortipediadata (data from hortipedia.com - more than 30,000 species with shape and environment parameters)
- edibleplants (about 500 species)
- celestefield (species and their coordinate on a field I bought in order to do tests in real life)

Satellites data aren't required for the first release.
