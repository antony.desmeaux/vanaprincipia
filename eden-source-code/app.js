const express = require("express");
const fs = require('fs');
const app = express();

// Create application/x-www-form-urlencoded parser
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })

// Use express (node.js framework) static function to translate path in html code
app.use(express.static(__dirname + '/front'));

var result_name;

app.listen(22022, () => {
    console.log("Listen on the port 22022...");
});

// cf. from https://expressjs.com/en/guide/routing.html
// Route path: /users/:userId/books/:bookId
// Request URL: http://localhost:3000/users/34/books/8989
// req.params: { "userId": "34", "bookId": "8989" }

// Routing: respond with index.html when a get is made to the /edenapp
// app.get(path, callback)
// path: The path for which the middleware function is being called
// callback: They can be a middleware function or a series/array of middleware functions.

app.get('/edenapp', function (req, res) { //res = response object
    res.sendFile(__dirname + '/front/index.html');
 })

 // On click, if enough information, do the math and send the result!
app.post('/edenapp', urlencodedParser, function (req, res) {

    if (req.body.field_name == '' || req.body.field_length == '' ||  req.body.field_width == '')
    {
        res.sendFile(__dirname + '/front/index.html' );
    }
    else 
    {
        //res.sendFile(__dirname + '/front/loading.html');
    
        // Prepare output in JSON format
        response = {
            field_name:req.body.field_name,
            field_length:req.body.field_length,
            field_width:req.body.field_width,
            exposure:req.body.exposure,
            moisture:req.body.moisture,
            soil:req.body.soil,
            hardiness:req.body.hardiness
        };
        console.log(response);
        var input = JSON.stringify(response);
    
        result_name = req.body.field_name;
        var file_name = result_name + '.json';
        var command_line = 'cargo run ' + file_name;
    
        fs.writeFile(file_name, input, (err) => { 
            if(err) {
                throw err;
            }
            else {
                console.log('Json file containing fields parameters has been created');
            }
        });
        
        // Execute the 'cargo run XXXX.json' command
        const { exec } = require("child_process");    
        exec(command_line, (error, stdout, stderr) => {
            console.log(`Running: ${command_line}`);
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
            }
            console.log(`stdout: ${stdout}`);

            // The image of the field has been created
            if (stderr != '') {
                console.log('Result is ready');
                return res.redirect('/result');
            }
            else {
                console.log('Result couldn\'t be computed');
                return;
            }
        });
    }
});

app.get('/result', function (req, res) { //res = response object
    res.setHeader('Content-Type', 'image/png');
    res.sendFile(__dirname + '/results/' + result_name + '.png');
});