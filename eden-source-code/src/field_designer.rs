//--------------------------------------------------------------------------------
// Used modules contained in the crate
//--------------------------------------------------------------------------------
use crate::main_structures::*;
use crate::field_designer;
use crate::tools;

//--------------------------------------------------------------------------------
// fn add_circle_coordinates_to_plants() {}
//
// Add coordinates to plant to be able to map them (circle design)
//--------------------------------------------------------------------------------
pub fn add_circle_coordinates_to_plants(plants_by_lifeform: PlantsByLifeform, available_coordinates: &mut Vec::<(i32,i32)>) -> PlantsByLifeformWithCoordinates {

    let lifeform_coordinates = set_circle_coordinates(available_coordinates);

    let mut field_plants_by_lifeform = tools::add_empty_coordinate_parameter_to_plants(&plants_by_lifeform);
    add_coordinates_to_plants(&mut field_plants_by_lifeform, &lifeform_coordinates);

    field_plants_by_lifeform
}

//--------------------------------------------------------------------------------
// fn design_circle() {}
//
//
//--------------------------------------------------------------------------------
pub fn design_circle(block_center_x_value: i32, block_center_y_value: i32, available_coordinates: &mut Vec::<(i32, i32)>, lifeform_coordinates: &mut LifeformCoordinates) {
    /*
    X X X X X
    X X X X X
    X X X X X
    X X X X X
    X X X X X
    */

    // lifeform = 1 - tree - (x, y)
    /*


        X 


    */
    let index = available_coordinates.iter().position(|x| *x == ((block_center_x_value),(block_center_y_value)));
    if index != None {
        lifeform_coordinates.trees_coordinates.push(((block_center_x_value),(block_center_y_value)));
        available_coordinates.remove(index.unwrap());
    }


    // lifeform = 2 - subshurb - (x-1, y), (x+1, y), (x, y-1), (x, y+1)
    /*

        X
      X O X
        X

    */
    let vec_subshurbs_coordinates = [
        ((block_center_x_value-1),(block_center_y_value)), 
        ((block_center_x_value+1),(block_center_y_value)), 
        ((block_center_x_value),(block_center_y_value-1)), 
        ((block_center_x_value),(block_center_y_value+1))
        ];
    for coordinate in vec_subshurbs_coordinates {
        let index = available_coordinates.iter().position(|x| *x == coordinate);
        if index != None {
            lifeform_coordinates.subshurbs_coordinates.push(coordinate);
            available_coordinates.remove(index.unwrap());
        }
    }

    // lifeform = 3 - shurb - (x-1, y+1), (x-1, y-1), (x+1, y-1), (x+1, y+1)
    /*

      X O X
      O O O
      X O X

    */
    let vec_shurbs_coordinates = [
        ((block_center_x_value-1),(block_center_y_value+1)), 
        ((block_center_x_value-1),(block_center_y_value-1)), 
        ((block_center_x_value+1),(block_center_y_value+1)), 
        ((block_center_x_value+1),(block_center_y_value-1))
        ];
    for coordinate in vec_shurbs_coordinates {
        let index = available_coordinates.iter().position(|x| *x == coordinate);
        if index != None {
            lifeform_coordinates.shurbs_coordinates.push(coordinate);
            available_coordinates.remove(index.unwrap());
        }
    }

    // lifeform = 4 - climbers - ?
    /*
        NOT YET
    */

    // lifeform = 5 - perennial - (x-2, y), (x+2, y), (x, y-2), (x, y+2)
    /*
        X
      O O O
    X O O O X
      O O O
        X
    */
    let vec_perennials_coordinates = [
        ((block_center_x_value-2),(block_center_y_value)), 
        ((block_center_x_value+2),(block_center_y_value)), 
        ((block_center_x_value),(block_center_y_value-2)), 
        ((block_center_x_value),(block_center_y_value+2))
        ];
    for coordinate in vec_perennials_coordinates {
        let index = available_coordinates.iter().position(|x| *x == coordinate);
        if index != None {
            lifeform_coordinates.perennials_coordinates.push(coordinate);
            available_coordinates.remove(index.unwrap());
        }
    }

    // lifeform = 6 - annual_biennal - (x-2, y+2), (x-2, y-2), (x+2, y-2), (x+2, y+2)
    /*
    X   O   X
      O O O
    O O O O O
      O O O
    X   O   X
    */
    let vec_annual_biennals_coordinates = [
        ((block_center_x_value-2),(block_center_y_value+2)), 
        ((block_center_x_value-2),(block_center_y_value-2)), 
        ((block_center_x_value+2),(block_center_y_value+2)), 
        ((block_center_x_value+2),(block_center_y_value-2))
        ];
    for coordinate in vec_annual_biennals_coordinates {
        let index = available_coordinates.iter().position(|x| *x == coordinate);
        if index != None {
            lifeform_coordinates.annual_biennals_coordinates.push(coordinate);
            available_coordinates.remove(index.unwrap());
        }
    }

    // lifeform = 7 - bulb - (x-1, y+2), (x-1, y-2), (x-2, y+1), (x-2, y-1), (x+1, y-2), (x+1, y+2), (x+2, y-1), (x+2, y+1)
    /*
    O X O X O
    X O O O X
    O O O O O
    X O O O X
    O X O X O
    */
    let vec_bulbs_coordinates = [
        ((block_center_x_value-1),(block_center_y_value+2)), 
        ((block_center_x_value-1),(block_center_y_value-2)),
        ((block_center_x_value-2),(block_center_y_value+1)),
        ((block_center_x_value-2),(block_center_y_value-1)),
        ((block_center_x_value+1),(block_center_y_value+2)),
        ((block_center_x_value+1),(block_center_y_value-2)),
        ((block_center_x_value+2),(block_center_y_value+1)),
        ((block_center_x_value+2),(block_center_y_value-1))
        ];
    for coordinate in vec_bulbs_coordinates {
        let index = available_coordinates.iter().position(|x| *x == coordinate);
        if index != None {
            lifeform_coordinates.bulbs_coordinates.push(coordinate);
            available_coordinates.remove(index.unwrap());
        }
    }

    // lifeform = 8 - grass - ?
    /*
        NOT YET
    */
}

//--------------------------------------------------------------------------------
// fn set_circle_coordinates() {}
//
// Set coordinates to lifeform to get a circle design
//--------------------------------------------------------------------------------
fn set_circle_coordinates(available_coordinates: &mut Vec::<(i32, i32)>) -> LifeformCoordinates {

    let mut lifeform_coordinates = LifeformCoordinates {
        trees_coordinates: Vec::<(i32, i32)>::new(),
        subshurbs_coordinates: Vec::<(i32, i32)>::new(),
        shurbs_coordinates: Vec::<(i32, i32)>::new(),
        climbers_coordinates: Vec::<(i32, i32)>::new(),
        perennials_coordinates: Vec::<(i32, i32)>::new(),
        annual_biennals_coordinates: Vec::<(i32, i32)>::new(),
        bulbs_coordinates: Vec::<(i32, i32)>::new(),
        grass_coordinates: Vec::<(i32, i32)>::new()
    };

    let mut block_number_x = 1;
    let mut block_number_y = 1;

    let last_x_value = available_coordinates.last().unwrap().0;
    let last_y_value = available_coordinates.last().unwrap().1;

    let block_size = 5;

    while (block_number_y*block_size)-1 <= last_y_value{
        while (block_number_x*block_size)-1 <= last_x_value {

            let block_center_x_value = (block_number_x*block_size)-3;
            let block_center_y_value = (block_number_y*block_size)-3;

            design_circle(block_center_x_value, block_center_y_value, available_coordinates, &mut lifeform_coordinates);

            block_number_x += 1;
        }
        block_number_x = 1;
        block_number_y += 1;
    }
    
    lifeform_coordinates
}

//--------------------------------------------------------------------------------
// fn add_coordinates_to_plants() {}
//
// 
//--------------------------------------------------------------------------------
pub fn add_coordinates_to_plants(field_plants_by_lifeform: &mut PlantsByLifeformWithCoordinates, lifeform_coordinates: &LifeformCoordinates) {
    
    let mut index_value = 0;

    if !field_plants_by_lifeform.trees.is_empty() {
        let number_of_tree_species = field_plants_by_lifeform.trees.len();

        for coordinate in &lifeform_coordinates.trees_coordinates {
            field_plants_by_lifeform.trees.get_mut(index_value).unwrap().coordinates.push(*coordinate);
            if index_value == number_of_tree_species - 1 {
                index_value = 0;
            }
            else {
                index_value += 1;
            }
        }
    }

    index_value = 0;
    
    if !field_plants_by_lifeform.subshurbs.is_empty() {
        let number_of_subshurb_species = field_plants_by_lifeform.subshurbs.len();

        for coordinate in &lifeform_coordinates.subshurbs_coordinates {
            field_plants_by_lifeform.subshurbs.get_mut(index_value).unwrap().coordinates.push(*coordinate);
            if index_value == number_of_subshurb_species - 1 {
                index_value = 0;
            }
            else {
                index_value += 1;
            }
        }
    }

    index_value = 0;
    
    if !field_plants_by_lifeform.shurbs.is_empty() {
        let number_of_shurb_species = field_plants_by_lifeform.shurbs.len();

        for coordinate in &lifeform_coordinates.shurbs_coordinates {
            field_plants_by_lifeform.shurbs.get_mut(index_value).unwrap().coordinates.push(*coordinate);
            if index_value == number_of_shurb_species - 1 {
                index_value = 0;
            }
            else {
                index_value += 1;
            }
        }
    }
    
    /*
    if !plants_by_lifeform.climbers.is_empty() {
    }
    */

    index_value = 0;
    
    if !field_plants_by_lifeform.perennials.is_empty() {
        let number_of_perennial_species = field_plants_by_lifeform.perennials.len();

        for coordinate in &lifeform_coordinates.perennials_coordinates {
            field_plants_by_lifeform.perennials.get_mut(index_value).unwrap().coordinates.push(*coordinate);
            if index_value == number_of_perennial_species - 1 {
                index_value = 0;
            }
            else {
                index_value += 1;
            }
        }
    }  
    
    index_value = 0;

    if !field_plants_by_lifeform.annual_biennals.is_empty() {
        let number_of_annual_biennal_species = field_plants_by_lifeform.annual_biennals.len();

        for coordinate in &lifeform_coordinates.annual_biennals_coordinates {
            field_plants_by_lifeform.annual_biennals.get_mut(index_value).unwrap().coordinates.push(*coordinate);
            if index_value == number_of_annual_biennal_species - 1 {
                index_value = 0;
            }
            else {
                index_value += 1;
            }
        }
    }

    index_value = 0;
    
    if !field_plants_by_lifeform.bulbs.is_empty() {
        let number_of_bulb_species = field_plants_by_lifeform.bulbs.len();

        for coordinate in &lifeform_coordinates.bulbs_coordinates {
            field_plants_by_lifeform.bulbs.get_mut(index_value).unwrap().coordinates.push(*coordinate);
            if index_value == number_of_bulb_species - 1 {
                index_value = 0;
            }
            else {
                index_value += 1;
            }
        }
    }
    
    /*
    if !plants_by_lifeform.grass.is_empty() {
    }
    */
}

//--------------------------------------------------------------------------------
// fn complete_tree_surrounding() {}
//
//--------------------------------------------------------------------------------
pub fn complete_tree_surrounding(available_coordinates: &mut Vec<(i32, i32)>, field_plants: &mut PlantsByLifeformWithCoordinates, lifeform_coordinates: &mut LifeformCoordinates) {
    let mut existing_plant = false;

    let mut tree_number = 1;

    for tree in &mut field_plants.trees {
            println!("--------------------------------------------------------------------------------");
            println!("Adding plants around the tree {}", tree.name);
            let tree_environment = tree.environment;
            let adapted_plants = tools::get_plants_from_environment(tree_environment);
            let adapted_plants_by_lifeform = tools::parse_plants_by_lifeform(adapted_plants);
            let mut adapted_plants_by_lifeform_with_coordinates = tools::add_empty_coordinate_parameter_to_plants(&adapted_plants_by_lifeform);

            for coordinate in &tree.coordinates {
                    println!("Tree #{} location is {:?}", tree_number, coordinate);
                    tools::clear_coordinates_vectors(&mut adapted_plants_by_lifeform_with_coordinates, lifeform_coordinates);
    
                    field_designer::design_circle(coordinate.0, coordinate.1, available_coordinates, lifeform_coordinates);
                    field_designer::add_coordinates_to_plants(&mut adapted_plants_by_lifeform_with_coordinates, lifeform_coordinates);

                    for subshurb_to_add in &mut adapted_plants_by_lifeform_with_coordinates.subshurbs {
                            if !subshurb_to_add.coordinates.is_empty() {
                                    for field_subshurb in &mut field_plants.subshurbs {
                                            if field_subshurb.name == subshurb_to_add.name {
                                                    println!("Adding coordinates {:?} to existing subshurb {}", subshurb_to_add.coordinates, subshurb_to_add.name);
                                                    field_subshurb.coordinates.append(&mut subshurb_to_add.coordinates);
                                                    existing_plant = true;
                                            }
                                    }

                                    if !existing_plant {
                                            field_plants.subshurbs.push(subshurb_to_add.clone());
                                            println!("Adding coordinates {:?} to the new subshurb {}", subshurb_to_add.coordinates, subshurb_to_add.name);
                                    }

                                    existing_plant = false;
                            }
                    }

                    for shurb_to_add in &mut adapted_plants_by_lifeform_with_coordinates.shurbs {
                            if !shurb_to_add.coordinates.is_empty() {
                                    for field_shurb in &mut field_plants.shurbs {
                                            if field_shurb.name == shurb_to_add.name {
                                                    println!("Adding coordinates {:?} to existing shurb {}", shurb_to_add.coordinates, shurb_to_add.name);
                                                    field_shurb.coordinates.append(&mut shurb_to_add.coordinates);
                                                    existing_plant = true;
                                            }
                                    }

                                    if !existing_plant {
                                            field_plants.shurbs.push(shurb_to_add.clone());
                                            println!("Adding coordinates {:?} to the new shurb {}", shurb_to_add.coordinates, shurb_to_add.name);
                                    }

                                    existing_plant = false;
                            }
                    }

                    for climber_to_add in &mut adapted_plants_by_lifeform_with_coordinates.climbers {
                            if !climber_to_add.coordinates.is_empty() {
                                    for field_climber in &mut field_plants.climbers {
                                            if field_climber.name == climber_to_add.name {
                                                    println!("Adding coordinates {:?} to existing climber {}", climber_to_add.coordinates, climber_to_add.name);
                                                    field_climber.coordinates.append(&mut climber_to_add.coordinates);
                                                    existing_plant = true;
                                            }
                                    }

                                    if !existing_plant {
                                            field_plants.climbers.push(climber_to_add.clone());
                                            println!("Adding coordinates {:?} to the new climbers {}", climber_to_add.coordinates, climber_to_add.name);
                                    }

                                    existing_plant = false;
                            }
                    }

                    for perennial_to_add in &mut adapted_plants_by_lifeform_with_coordinates.perennials {
                            if !perennial_to_add.coordinates.is_empty() {
                                    for field_perennial in &mut field_plants.perennials {
                                            if field_perennial.name == perennial_to_add.name {
                                                    println!("Adding coordinates {:?} to existing perennial {}", perennial_to_add.coordinates, perennial_to_add.name);
                                                    field_perennial.coordinates.append(&mut perennial_to_add.coordinates);
                                                    existing_plant = true;
                                            }
                                    }

                                    if !existing_plant {
                                            field_plants.perennials.push(perennial_to_add.clone());
                                            println!("Adding coordinates {:?} to the new perennials {}", perennial_to_add.coordinates, perennial_to_add.name);
                                    }

                                    existing_plant = false;
                            }
                    }

                    for annual_biennal_to_add in &mut adapted_plants_by_lifeform_with_coordinates.annual_biennals {
                            if !annual_biennal_to_add.coordinates.is_empty() {
                                    for field_annual_biennal in &mut field_plants.annual_biennals {
                                            if field_annual_biennal.name == annual_biennal_to_add.name {
                                                    println!("Adding coordinates {:?} to existing annual_biennal {}", annual_biennal_to_add.coordinates, annual_biennal_to_add.name);
                                                    field_annual_biennal.coordinates.append(&mut annual_biennal_to_add.coordinates);
                                                    existing_plant = true;
                                            }
                                    }

                                    if !existing_plant {
                                            field_plants.annual_biennals.push(annual_biennal_to_add.clone());
                                            println!("Adding coordinates {:?} to the new annual_biennal {}", annual_biennal_to_add.coordinates, annual_biennal_to_add.name);
                                    }

                                    existing_plant = false;
                            }
                    }

                    for bulb_to_add in &mut adapted_plants_by_lifeform_with_coordinates.bulbs {
                            if !bulb_to_add.coordinates.is_empty() {
                                    for field_bulb in &mut field_plants.bulbs {
                                            if field_bulb.name == bulb_to_add.name {
                                                    println!("Adding coordinates {:?} to existing bulb {}", bulb_to_add.coordinates, bulb_to_add.name);
                                                    field_bulb.coordinates.append(&mut bulb_to_add.coordinates);
                                                    existing_plant = true;
                                            }
                                    }

                                    if !existing_plant {
                                            field_plants.bulbs.push(bulb_to_add.clone());
                                            println!("Adding coordinates {:?} to the new bulb {}", bulb_to_add.coordinates, bulb_to_add.name);
                                    }

                                    existing_plant = false;
                            }
                    }

                    for grass_to_add in &mut adapted_plants_by_lifeform_with_coordinates.grass {
                            if !grass_to_add.coordinates.is_empty() {
                                    for field_grass in &mut field_plants.grass {
                                            if field_grass.name == grass_to_add.name {
                                                    println!("Adding coordinates {:?} to existing grass {}", grass_to_add.coordinates, grass_to_add.name);
                                                    field_grass.coordinates.append(&mut grass_to_add.coordinates);
                                                    existing_plant = true;
                                            }
                                    }

                                    if !existing_plant {
                                            field_plants.grass.push(grass_to_add.clone());
                                            println!("Adding coordinates {:?} to the new grass {}", grass_to_add.coordinates, grass_to_add.name);
                                    }

                                    existing_plant = false;
                            }
                    }

                    tree_number += 1;
                    println!("--------------------------------------------------------------------------------");
            }

            tree_number = 1;
    }
}
