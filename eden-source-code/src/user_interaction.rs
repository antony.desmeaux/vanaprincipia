
//--------------------------------------------------------------------------------
// Modules contained in this crate
//--------------------------------------------------------------------------------
use crate::main_structures;
use crate::tools;

//--------------------------------------------------------------------------------
// fn get_plants_from_user_environment() {}
//
//
//--------------------------------------------------------------------------------
pub fn get_plants_from_user_environment() -> Vec<main_structures::Plant> {
  let mut plants;
  let mut user_environment: main_structures::Environment;

  loop {
          let mut user_exposure = String::new();
          println!("Exposure, values are 1 (full sun), 12, 123, 2, 23, 3 (full shade): ");
          std::io::stdin().read_line(&mut user_exposure).unwrap();

          let mut user_moisture = String::new();
          println!("Moisture, values are 1 (dry), 12, 13, 14, 15, 2, 23, ..., 4, 45, 5 (aquatic plant): ");
          std::io::stdin().read_line(&mut user_moisture).unwrap();

          let mut user_soil = String::new();
          println!("Soil, values are 1 (light soil), 12, 123, 2, 23, 3, 34, 4 (peaty soil): ");
          std::io::stdin().read_line(&mut user_soil).unwrap();

          let mut user_hardiness = String::new();
          println!("Hardiness, values are 1 (below -45°C), 2, 3, 4, 5, 6, 7, 8, 9, 10 (at least 1°C): ");
          std::io::stdin().read_line(&mut user_hardiness).unwrap();

          user_environment = main_structures::Environment {
                  exposure: user_exposure.trim().parse::<i32>().unwrap(), 
                  moisture: user_moisture.trim().parse::<i32>().unwrap(), 
                  soil: user_soil.trim().parse::<i32>().unwrap(), 
                  hardiness: user_hardiness.trim().parse::<i32>().unwrap()
          };

          plants = tools::get_plants_from_environment(user_environment);

          if !plants.is_empty() {
                  break;
          }
          else {
                  println!("Cannot find any plant with these environment parameters, try again please.");
          }
      }

      plants
}

//--------------------------------------------------------------------------------
// fn ask_field_parameters() {}
//
//
//--------------------------------------------------------------------------------
pub fn ask_field_parameters() -> (String, main_structures::FieldSize) {
    let mut field_name = String::new();
    println!("Field name: "); //celestefield
    std::io::stdin().read_line(&mut field_name).unwrap();
    field_name = field_name.trim().to_string();

    let mut field_length = String::new();
    println!("Field length: ");
    std::io::stdin().read_line(&mut field_length).unwrap();
    field_length = field_length.trim().to_string();

    let mut field_width = String::new();
    println!("Field width: ");
    std::io::stdin().read_line(&mut field_width).unwrap();
    field_width = field_width.trim().to_string();

    let field_size: main_structures::FieldSize = main_structures::FieldSize{length: field_length.parse::<i32>().unwrap(), width: field_width.parse::<i32>().unwrap()};

    (field_name, field_size)
}

//--------------------------------------------------------------------------------
// fn ask_database_parameters() {}
//
//
//--------------------------------------------------------------------------------
pub fn ask_database_parameters() -> (String, String, String, String, String) {
    let mut field_name = String::new();
    println!("Database name: "); //celestefield
    std::io::stdin().read_line(&mut field_name).unwrap();
    field_name = field_name.trim().to_string();

    let mut plant_name = String::new();
    println!("Plant scientific name (format = Xxxx_xxxx): ");
    std::io::stdin().read_line(&mut plant_name).unwrap();
    plant_name = plant_name.trim().to_string();

    let mut plant_common_name = String::new();
    println!("Plant common name. Press enter if useless: ");
    std::io::stdin().read_line(&mut plant_common_name).unwrap();
    plant_common_name = plant_common_name.trim().to_string();

    let mut plant_coordinates = String::new();
    println!("Plant coordinates (x1;y1) (x2;y2) (x3;y3) (...). Press enter if useless: ");
    std::io::stdin().read_line(&mut plant_coordinates).unwrap();
    plant_coordinates = plant_coordinates.trim().to_string();

    let mut another_plant = String::new();
    println!("Do you want to add another plant ? (Y/N): ");
    std::io::stdin().read_line(&mut another_plant).unwrap();
    another_plant = another_plant.trim().to_string();

    (field_name, plant_name, plant_coordinates, another_plant, plant_common_name)
}

//--------------------------------------------------------------------------------
// fn display_choice_screen() {}
//
//
//--------------------------------------------------------------------------------
pub fn display_choice_screen(user_input: &mut String) {
    println!("
             
                                           *▓▓▄▄, ╦∩
                                              `╙╙▓▀▄▄,
                                                ╚▀╕`²▀▓▓^
                                               ▄
                                              ▄Ñ
                                             ▓▌          ▓▓▓▓▄,
                                          'T▓▓  ╓▄▓▄▄ ,╓▓▓▓▓▓▓▓▓▓µ
                                           ▓▓H╩▓▌ ▀▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▄,
                                         `▓▓▓▄▄▄╓▄▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▄,
                                       , ▐▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒
                                      é▓▓▓▓▓ ╙,╙▓▓▓ `╙▀▓▓╙▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓µ
                                     ╓@▄▄▓▓▓ ¬` ▀ 4▓▓▓@▓▓▓ ╫▓▓▓▓▓▓▓▓▓▓▓▓╫▓µ
                                 ,╓@╫╫╫╫▓▓▓█@. ┌,    ▓╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫▓▓░
                                ╓ÑÑÑÑÑÑÑ▓▓▓█╫ÑÑÑÑÑÑ▓@╫ÑÑÑÑ╙╝╫ÑÑÑÑÑÑÑÑÑÑÑÑ▒▓▒
                                ÑÑÑÑÑÑÑÑ▓▓▓▓▒ÑÑÑÑÑÑÑ╫ ╚ÑÑÑ@µ╓µ ``ª╫ÑÑÑÖ `╫▓▒░
                                ▒▒▒▒▒▒▒▒▓▓▓▓▌▒▒▒▒▒▒▒▒N ╙▒▒▒▒▒M     ╫▒M    ▓▓▒
                                └╫ÑÑÑÑÑÑ▓▓▓▓▓▒ÑÑÑÑÑÑÑÑ@ ╚╫M╨       `╫     ▓▓▒
                                  ``''²╫▒▓▓▓▓▓ÑÑÑ╫╫╫╫╫╫╫w╖╗H             ▐▓▒░
                                        J▓▓▓▓╫▓╫╫╫╫╫╫╫╫╫╫╫╜              ▓▓▒    ⌐
                                         ▓▓▓Ñ▒▒╫╫╫╫╫╫╫╫▓╜               ▓▓Ñ▒ ,#`
                                          ▓▓▒▒▒▒▒▓▓▓▓▓Ñ               ,▓▓╫▒#╫╜
                                         ,▓▓@▒▒▒▒▒╫▓▓▓               ▄▓▀Ñ╫M`
                              `~         ▓▓▓▓▓▒▒▒▒▒▒▒Ñ,,▄M      ,,╓m╫▒Ñ╫╜
                                `«        ▓▓▓▓▓▓▄▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╫Ñ▒
                                  `ª╥,     ▀▓▓▓▓▓╜ `ªº╨╩N▒N╩╩▒▄▓▓▓╫▒░
                                     `╨Mæ╖,             ,╓▄▓▓▓▀Ñ▒╩`
                                         `╨╩▀▀▓▓▓▓▓▓▓▓▓▀▀▀▀Ñ╩╩'
                                               ```'''''``
        
                                   ███████╗██████╗░███████╗███╗░░██╗
                                   ██╔════╝██╔══██╗██╔════╝████╗░██║
                                   █████╗░░██║░░██║█████╗░░██╔██╗██║
                                   ██╔══╝░░██║░░██║██╔══╝░░██║╚████║
                                   ███████╗██████╔╝███████╗██║░╚███║
                                   ╚══════╝╚═════╝░╚══════╝╚═╝░░╚══╝

    ");
    println!("--------------------------------------------------------------------------------");
    println!("Type '1' to generate the map from an existing field");
    println!("Type '2' to generate a map from a random environment");
    println!("Type '3' to generate a map from your environment parameters");
    println!("Type '4' to complete with adapted species the map of an existing field");
    println!("--------------------------------------------------------------------------------");
    println!("-------- DATABASE PART ---------------------------------------------------------");
    println!("--------------------------------------------------------------------------------");
    println!("Type '5' to update coordinates of a plant in the database");
    println!("Type '6' to remove plants from the database");
    println!("Type '7' to add plants to the database");
    println!("--------------------------------------------------------------------------------");
    println!("Your answer is: ");
    std::io::stdin().read_line(user_input).unwrap();
}