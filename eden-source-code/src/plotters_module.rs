//--------------------------------------------------------------------------------
// Used modules contained in the crate
//--------------------------------------------------------------------------------
use crate::main_structures;
use crate::tools;

use plotters::prelude::*;
use plotters::coord::types::RangedCoordi32;
use std::fs;

//--------------------------------------------------------------------------------
// fn draw_map_of_lifeform_characteristics() {}
//
// Draw plots based on lifeform of field's plants
//--------------------------------------------------------------------------------
pub fn draw_map_of_lifeform_characteristics(file_name: &String, field_plants: &mut main_structures::PlantsByLifeformWithCoordinates, field_size: &main_structures::FieldSize) -> Result<(), Box<dyn std::error::Error>> {

    let output_file_path = &format!("results/{}.png", file_name);
    fs::create_dir_all("results")?;

    //Set global parameters of the output map
    let pixel_enhancement = 300;

    let root_area = BitMapBackend::new(
        output_file_path, 
        (
            (field_size.length*pixel_enhancement).try_into().unwrap(), 
            (field_size.width*pixel_enhancement).try_into().unwrap()
        )
    ).into_drawing_area();

    let root_area = root_area.apply_coord_spec(Cartesian2d::<RangedCoordi32, RangedCoordi32>::new(
        0..field_size.length,
        0..field_size.width,
        (0..field_size.length*pixel_enhancement, 0..field_size.width*pixel_enhancement)
    ));

    let dot_and_label = |(x, y),plant_name: String,environment_parameters: main_structures::EnvironmentTranslated,color: &RGBColor,dot_size: i32,soil_type| {
        
        let mut multi_lines = MultiLineText::<(i32, i32),String>::new ((20, 20),("sans-serif", 10).into_font());
        multi_lines.push_line(format!("Name: {}", plant_name).replace("_", " "));
        multi_lines.push_line(format!("Exposure: {}", environment_parameters.exposure));
        multi_lines.push_line(format!("Moisture: {}", environment_parameters.moisture));
        multi_lines.push_line(format!("Soil: {}", environment_parameters.soil));
        multi_lines.push_line(format!("Hardiness: {}", environment_parameters.hardiness));

        let soil_color;
        if soil_type == 1 {//light = yellow
            soil_color =  &RGBColor(245,218,182);
        }
        else if soil_type == 2 {//medium = light brown
            soil_color =  &RGBColor(218,178,127);
        }
        else if soil_type == 3 {//heavy = darkish brown
            soil_color =  &RGBColor(188,140,78);
        }
        else if soil_type == 4 {//peaty = dark green
            soil_color =  &RGBColor(245,218,182);
        }
        else {
            soil_color =  &RGBColor(255,255,255);
        }

        return EmptyElement::at((x, y))
            + Rectangle::new([(-pixel_enhancement/2,-pixel_enhancement/2),(pixel_enhancement/2,pixel_enhancement/2)], ShapeStyle::from(soil_color).filled())
            + Circle::new((0, 0), dot_size, ShapeStyle::from(color).filled())
            + multi_lines
    };

    let other_area = |(x, y), color: &RGBColor, rectangle_size: i32| {
        return EmptyElement::at((x, y))
            + Rectangle::new([(-rectangle_size/2,-rectangle_size/2),(rectangle_size/2,rectangle_size/2)], ShapeStyle::from(color).filled());
    };

    root_area.fill(&WHITE)?;

    for path_element in &field_plants.path {
        root_area.draw(&other_area(
            path_element.clone(), 
            &RGBColor(230,230,230), 
            pixel_enhancement))?;
    }

    for tree in &field_plants.trees {
        for coordinate in &tree.coordinates {
            root_area.draw(&dot_and_label(
                coordinate.clone(),
                tree.name.clone(),
                tools::environment_parameters_translator(tree.environment),
                &RGBColor(0,102,0),
                20,
                tree.environment.soil
            ))?;
        }
        if tree.coordinates.is_empty() {
            println!("No coordinate found for tree {}", tree.name);
        }    
    }

    for subshurb in &field_plants.subshurbs{
        for coordinate in &subshurb.coordinates {
            root_area.draw(&dot_and_label(
                coordinate.clone(), 
                subshurb.name.clone(), 
                tools::environment_parameters_translator(subshurb.environment), 
                &RGBColor(0,153,0), 
                15,
                subshurb.environment.soil
            ))?;
        }
        if subshurb.coordinates.is_empty() {
            println!("No coordinate found for subshurb {}", subshurb.name);
        }    
    }

    for shurb in &field_plants.shurbs{
        for coordinate in &shurb.coordinates {
            root_area.draw(&dot_and_label(
                coordinate.clone(), 
                shurb.name.clone(), 
                tools::environment_parameters_translator(shurb.environment), 
                &RGBColor(0,204,0), 
                10,
                shurb.environment.soil
            ))?;
        }
        if shurb.coordinates.is_empty() {
            println!("No coordinate found for shurb {}", shurb.name);
        }    
    }

    for climber in &field_plants.climbers{
        for coordinate in &climber.coordinates {
            root_area.draw(&dot_and_label(
                coordinate.clone(), 
                climber.name.clone(), 
                tools::environment_parameters_translator(climber.environment), 
                &RGBColor(153,76,0), 
                2,
                climber.environment.soil
            ))?;
        }
        if climber.coordinates.is_empty() {
            println!("No coordinate found for climber {}", climber.name);
        }
    }

    for perennial in &field_plants.perennials{
        for coordinate in &perennial.coordinates {
            root_area.draw(&dot_and_label(
                coordinate.clone(), 
                perennial.name.clone(), 
                tools::environment_parameters_translator(perennial.environment), 
                &RGBColor(0,255,0), 
                5,
                perennial.environment.soil
            ))?;
        }
        if perennial.coordinates.is_empty() {
            println!("No coordinate found for perennial {}", perennial.name);
        }    
    }

    for annual_biennal in &field_plants.annual_biennals{
        for coordinate in &annual_biennal.coordinates {
            root_area.draw(&dot_and_label(
                coordinate.clone(), 
                annual_biennal.name.clone(), 
                tools::environment_parameters_translator(annual_biennal.environment), 
                &RGBColor(0,128,255), 
                4,
                annual_biennal.environment.soil
            ))?;
        }
        if annual_biennal.coordinates.is_empty() {
            println!("No coordinate found for annual_biennal {}", annual_biennal.name);
        }    
    }

    for bulb in &field_plants.bulbs{
        for coordinate in &bulb.coordinates {
            root_area.draw(&dot_and_label(
                coordinate.clone(), 
                bulb.name.clone(), 
                tools::environment_parameters_translator(bulb.environment), 
                &RGBColor(102,0,102), 
                3,
                bulb.environment.soil
            ))?;
        }
        if bulb.coordinates.is_empty() {
            println!("No coordinate found for bulb {}", bulb.name);
        }   
    }

    for grass in &field_plants.grass{
        for coordinate in &grass.coordinates {
            root_area.draw(&dot_and_label(
                coordinate.clone(), 
                grass.name.clone(), 
                tools::environment_parameters_translator(grass.environment), 
                &RGBColor(102,0,102), 
                1,
                grass.environment.soil
            ))?;
        }
        if grass.coordinates.is_empty() {
            println!("No coordinate found for grass {}", grass.name);
        }    
    }

    println!("Plotting on {}.png in progress...", file_name);

    Ok(())
}