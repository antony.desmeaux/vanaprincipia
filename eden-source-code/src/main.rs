//--------------------------------------------------------------------------------                                                                      
//     
//                                   *▓▓▄▄, ╦∩
//                                      `╙╙▓▀▄▄,
//                                        ╚▀╕`²▀▓▓^
//                                       ▄
//                                      ▄Ñ
//                                     ▓▌          ▓▓▓▓▄,
//                                  'T▓▓  ╓▄▓▄▄ ,╓▓▓▓▓▓▓▓▓▓µ
//                                   ▓▓H╩▓▌ ▀▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▄,
//                                 `▓▓▓▄▄▄╓▄▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▄,
//                               , ▐▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒
//                              é▓▓▓▓▓ ╙,╙▓▓▓ `╙▀▓▓╙▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓µ
//                             ╓@▄▄▓▓▓ ¬` ▀ 4▓▓▓@▓▓▓ ╫▓▓▓▓▓▓▓▓▓▓▓▓╫▓µ
//                         ,╓@╫╫╫╫▓▓▓█@. ┌,    ▓╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫╫▓▓░
//                        ╓ÑÑÑÑÑÑÑ▓▓▓█╫ÑÑÑÑÑÑ▓@╫ÑÑÑÑ╙╝╫ÑÑÑÑÑÑÑÑÑÑÑÑ▒▓▒
//                        ÑÑÑÑÑÑÑÑ▓▓▓▓▒ÑÑÑÑÑÑÑ╫ ╚ÑÑÑ@µ╓µ ``ª╫ÑÑÑÖ `╫▓▒░
//                        ▒▒▒▒▒▒▒▒▓▓▓▓▌▒▒▒▒▒▒▒▒N ╙▒▒▒▒▒M     ╫▒M    ▓▓▒
//                        └╫ÑÑÑÑÑÑ▓▓▓▓▓▒ÑÑÑÑÑÑÑÑ@ ╚╫M╨       `╫     ▓▓▒
//                          ``''²╫▒▓▓▓▓▓ÑÑÑ╫╫╫╫╫╫╫w╖╗H             ▐▓▒░
//                                J▓▓▓▓╫▓╫╫╫╫╫╫╫╫╫╫╫╜              ▓▓▒    ⌐
//                                 ▓▓▓Ñ▒▒╫╫╫╫╫╫╫╫▓╜               ▓▓Ñ▒ ,#`
//                                  ▓▓▒▒▒▒▒▓▓▓▓▓Ñ               ,▓▓╫▒#╫╜
//                                 ,▓▓@▒▒▒▒▒╫▓▓▓               ▄▓▀Ñ╫M`
//                      `~         ▓▓▓▓▓▒▒▒▒▒▒▒Ñ,,▄M      ,,╓m╫▒Ñ╫╜
//                        `«        ▓▓▓▓▓▓▄▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╫Ñ▒
//                          `ª╥,     ▀▓▓▓▓▓╜ `ªº╨╩N▒N╩╩▒▄▓▓▓╫▒░
//                             `╨Mæ╖,             ,╓▄▓▓▓▀Ñ▒╩`
//                                 `╨╩▀▀▓▓▓▓▓▓▓▓▓▀▀▀▀Ñ╩╩"
//                                       ```'''''``
//
//                           ███████╗██████╗░███████╗███╗░░██╗
//                           ██╔════╝██╔══██╗██╔════╝████╗░██║
//                           █████╗░░██║░░██║█████╗░░██╔██╗██║
//                           ██╔══╝░░██║░░██║██╔══╝░░██║╚████║
//                           ███████╗██████╔╝███████╗██║░╚███║
//                           ╚══════╝╚═════╝░╚══════╝╚═╝░░╚══╝
//
// Version: 0.2
// Created on April 2020, last update on 07/03/2023 by Tonio
//
// This program intend to provide to anyone the right plants architecture
// for her/his environment. Open-source, it intends to evolve through time to 
// be as clear as possible (easier to maintain, easier to improve).
// Enjoy the ride! This is for Earth's living creatures.
//
//--------------------------------------------------------------------------------

use crate::main_structures::LifeformCoordinates;
use prse::parse;

use std::env;
use std::fs;

//--------------------------------------------------------------------------------
// Modules contained in this crate
//--------------------------------------------------------------------------------
pub mod database_actions;
pub mod plotters_module;
pub mod main_structures;
pub mod tools;
pub mod field_designer;
pub mod user_interaction;

//--------------------------------------------------------------------------------
// To run this program (or any Rust program), execute the following steps:
//      1. import the mySQL database to your system
//              - get it from the dump: vanaprincipia_database
//      2. from the terminal, go to the vana-source-code directory
//      3. run the "build cargo" command to build the program
//      4. run the "run cargo" command to run the program
//      5. outputs are located in the "results" directory
//--------------------------------------------------------------------------------

fn main() -> Result<(), Box<dyn std::error::Error>> {

        //--------------------------------------------------------------------------------
        // Beginning of the Eden program
        //--------------------------------------------------------------------------------

        let args: Vec<String> = env::args().collect();

        if args.len() > 1 {
                //AUTOMATIC: PROGRAM STARTED FROM THE WEB UI
                /*The parameter will look like that
                {"field_name":"Test","field_length":"20","field_width":"30","exposure":"1","moisture":"34","soil":"3","hardiness":"9"}
                */

                let input_from_ui = &args[1];  
                let file_content = fs::read_to_string(&input_from_ui).expect("Unable to read the file");
                println!("{} content is:\n{}", input_from_ui, file_content);

                let field_name: &str;
                let field_length: &str;
                let field_width: &str;
                let exposure: &str;
                let moisture: &str;
                let soil: &str;
                let hardiness: &str;

                parse!(file_content, "{{\"field_name\":\"{field_name}\",\"field_length\":\"{field_length}\",\"field_width\":\"{field_width}\",\"exposure\":\"{exposure}\",\"moisture\":\"{moisture}\",\"soil\":\"{soil}\",\"hardiness\":\"{hardiness}\"}}");
                
                let user_exposure: String = exposure.to_string();
                let user_moisture: String = moisture.to_string();
                let user_soil: String = soil.to_string();
                let user_hardiness = hardiness.to_string();

                println!("Parameters are: exposure = {}, moisture = {}, soil = {}, hardiness = {}", user_exposure, user_moisture, user_soil, user_hardiness);

                let user_environment = main_structures::Environment {
                        exposure: user_exposure.trim().parse::<i32>().unwrap(), 
                        moisture: user_moisture.trim().parse::<i32>().unwrap(), 
                        soil: user_soil.trim().parse::<i32>().unwrap(), 
                        hardiness: user_hardiness.trim().parse::<i32>().unwrap()
                };
      
                let plants = tools::get_plants_from_environment(user_environment);
      
                if plants.is_empty() {
                        println!("Cannot find any plant with these environment parameters, try again please.");
                        std::process::abort()
                }

                let plants_by_lifeform = tools::parse_plants_by_lifeform(plants);

                let field_size: main_structures::FieldSize = main_structures::FieldSize{length: field_length.parse::<i32>().unwrap(), width: field_width.parse::<i32>().unwrap()};

                let mut available_coordinates = tools::generate_available_coordinates(&field_size);

                // PLOT A PATH
                //let path_coordinates = tools::get_random_path_coordinates(&field_size);
                //tools::remove_input_coordinates(&mut available_coordinates, &path_coordinates);
                //field_plants.path = path_coordinates;
                        
                let mut field_plants = field_designer::add_circle_coordinates_to_plants(plants_by_lifeform, &mut available_coordinates);

                // OUTPUT: adapted plants parameters plotted on a 2D map
                plotters_module::draw_map_of_lifeform_characteristics(&field_name.trim().to_string(), &mut field_plants, &field_size)
        }
        else {
                // MANUAL: PROGRAM STARTED FROM THE TERMINAL
                let mut user_input = String::new();

                user_interaction::display_choice_screen(&mut user_input);

                if user_input.trim() == String::from('1') {
                        //--------------------------------------------------------------------------------
                        // CASE 1: in the database, we got a "celestefield" (= field_name) table with 
                        // plants & their coordinates. We want to plot them on a 2D map.
                        //--------------------------------------------------------------------------------

                        let (field_name, field_size) = user_interaction::ask_field_parameters();

                        let mut field_plants = database_actions::get_field_plants(&field_name).unwrap();

                        // OUTPUT: plants parameters plotted on a 2D map
                        plotters_module::draw_map_of_lifeform_characteristics(&field_name.trim().to_string(), &mut field_plants, &field_size)
                }
                else if user_input.trim() == String::from('2') {
                        //--------------------------------------------------------------------------------
                        // CASE 2: get adapted plants to a random environment and plot them on a 2D map
                        //--------------------------------------------------------------------------------

                        let (field_name, field_size) = user_interaction::ask_field_parameters();

                        let plants = tools::get_plants_from_random_environment();
                        let plants_by_lifeform = tools::parse_plants_by_lifeform(plants);

                        let mut available_coordinates = tools::generate_available_coordinates(&field_size);

                        // PLOT A PATH
                        //let path_coordinates = tools::get_random_path_coordinates(&field_size);
                        //tools::remove_input_coordinates(&mut available_coordinates, &path_coordinates);
                        //field_plants.path = path_coordinates;
                        
                        let mut field_plants = field_designer::add_circle_coordinates_to_plants(plants_by_lifeform, &mut available_coordinates);

                        // OUTPUT: adapted plants parameters plotted on a 2D map
                        plotters_module::draw_map_of_lifeform_characteristics(&field_name.trim().to_string(), &mut field_plants, &field_size)
                }
                else if user_input.trim() == String::from('3') {
                        //--------------------------------------------------------------------------------
                        // CASE 3: get adapted plants to user environment and plot them on a 2D map
                        //--------------------------------------------------------------------------------

                        let (field_name, field_size) = user_interaction::ask_field_parameters();
                
                        let plants = user_interaction::get_plants_from_user_environment();

                        let plants_by_lifeform = tools::parse_plants_by_lifeform(plants);

                        let mut available_coordinates = tools::generate_available_coordinates(&field_size);

                        // PLOT A PATH
                        //let path_coordinates = tools::get_random_path_coordinates(&field_size);
                        //tools::remove_input_coordinates(&mut available_coordinates, &path_coordinates);
                        //field_plants.path = path_coordinates;
                        
                        let mut field_plants = field_designer::add_circle_coordinates_to_plants(plants_by_lifeform, &mut available_coordinates);

                        // OUTPUT: adapted plants parameters plotted on a 2D map
                        plotters_module::draw_map_of_lifeform_characteristics(&field_name.trim().to_string(), &mut field_plants, &field_size)
                }
                else if user_input.trim() == String::from('4') {
                        //--------------------------------------------------------------------------------
                        // CASE 4:  complete existing field with adapted plants
                        //--------------------------------------------------------------------------------

                        let (field_name, field_size) = user_interaction::ask_field_parameters();

                        let mut field_plants = database_actions::get_field_plants(&field_name).unwrap();

                        let field_plants_names = tools::extract_plants_name(&field_plants);
                        let existing_coordinates = tools::extract_existing_plants_coordinates(&field_plants);
                        let mut available_coordinates = tools::generate_available_coordinates(&field_size);

                        tools::remove_input_coordinates(&mut available_coordinates, &existing_coordinates);

                        let mut lifeform_coordinates = LifeformCoordinates {
                                trees_coordinates: Vec::<(i32, i32)>::new(),
                                subshurbs_coordinates: Vec::<(i32, i32)>::new(),
                                shurbs_coordinates: Vec::<(i32, i32)>::new(),
                                climbers_coordinates: Vec::<(i32, i32)>::new(),
                                perennials_coordinates: Vec::<(i32, i32)>::new(),
                                annual_biennals_coordinates: Vec::<(i32, i32)>::new(),
                                bulbs_coordinates: Vec::<(i32, i32)>::new(),
                                grass_coordinates: Vec::<(i32, i32)>::new()
                        };

                        field_designer::complete_tree_surrounding(&mut available_coordinates, &mut field_plants, &mut lifeform_coordinates);

                        // OUTPUT: plants parameters plotted on a 2D map
                        let file_name = format!("{}_updated_edible", field_name);
                        tools::save_field_plant_in_file(&field_plants, &field_plants_names);
                        plotters_module::draw_map_of_lifeform_characteristics(&file_name.trim().to_string(), &mut field_plants, &field_size)
                }
                else if user_input.trim() == String::from('5') {
                        loop {
                                let (database_name, plant_name, plant_coordinates, another_plant, plant_common_name) = user_interaction::ask_database_parameters();
                                let result = database_actions::update_plant_coordinates_of_field_database(&database_name, &plant_name, &plant_coordinates, &plant_common_name);

                                if result.err().is_some() {
                                        println!("Error during SQL request");
                                }
                                
                                if another_plant == "N" {
                                        break;
                                }
                        }

                        Ok(())
                }
                else if user_input.trim() == String::from('6') {
                        loop {
                                let (database_name, plant_name, _plant_coordinates, another_plant, _plant_common_name) = user_interaction::ask_database_parameters();
                                let result = database_actions::remove_plant_from_field_database(&database_name, &plant_name);

                                if result.err().is_some() {
                                        println!("Error during SQL request");
                                }

                                if another_plant == "N" {
                                        break;
                                }
                        }

                        Ok(())
                }
                else if user_input.trim() == String::from('7') {
                        loop {
                                let (database_name, plant_name, plant_coordinates, another_plant, _plant_common_name) = user_interaction::ask_database_parameters();
                                let result = database_actions::add_plant_to_field_database(&database_name, &plant_name, &plant_coordinates);

                                if result.err().is_some() {
                                        println!("Error during SQL request");
                                }
                                
                                if another_plant == "N" {
                                        break;
                                }
                        }

                        Ok(())
                }
                else {
                        println!("Sorry, {} is not an option yet!", user_input);

                        Ok(())
                }
        }

        //--------------------------------------------------------------------------------
        // Create the interfaces to conect to the available apps
        //      - monstera (https://gitlab.com/hi.soulier/monstera-api)
        //      - growingup (https://gitlab.com/thomas.devllp/growingup)
        //      - plantarium (https://gitlab.com/PierreAntona/Plantarium)
        //      - candide (https://gitlab.com/manon.lavenier/candide-project)
        //      - iris (https://gitlab.com/Yutsu/iris)
        //      - kalmia (https://gitlab.com/lucas.yvernaux/projet-kalmia)
        //      - origan (https://gitlab.com/FannyMaille/origan-vanaprincipia)
        //--------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------
        // TEST - Earth Observation (EO) part, not useful for now 
        // (+ issue with coordinates argument '-c 4.484911,48.409947:4.545078,48.377347')
        //--------------------------------------------------------------------------------

        //copernicus_script::run_copernicus_script();
}