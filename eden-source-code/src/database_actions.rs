//--------------------------------------------------------------------------------
// Used modules contained in the crate
//--------------------------------------------------------------------------------
use crate::main_structures;
use crate::tools;
use crate::main_structures::Environment;

use mysql::*;
use mysql::prelude::*;

//--------------------------------------------------------------------------------
// fn connect_to_db_vanaprincipia() {}
//
// Connect to the postgreSQL database on the executing computer
// (needs to be up and running first)
//--------------------------------------------------------------------------------
pub fn connect_to_db_vanaprincipia() -> Result<PooledConn> {

    // WARNING: to be adpated to your local database (TODO: on a dedicated server)
    let mysql_url = "mysql://antony:connect-to-vana@localhost:3306/vana";

    // On server
    //let mysql_url = "mysql://rusty:That-one-is-easy-br0@localhost:3306/vana";

    // I don't remember the purpose of this one...
    //let mysql_url = "mysql://vanaprnantony:ConnectToV4n4@vanaprnantony.mysql.db/vanaprnantony";

    let pool = Pool::new(mysql_url)?;

    pool.get_conn()
}

//--------------------------------------------------------------------------------
// fn update_plant_coordinates_of_field_database() {}
//
//--------------------------------------------------------------------------------
pub fn update_plant_coordinates_of_field_database(database_name: &String, plant_name: &String, plant_coordinates: &String, plant_common_name: &String) -> std::result::Result<(), mysql::Error> {
    let client = connect_to_db_vanaprincipia();
    let update_query = format!("UPDATE {} SET coordinates = :plant_coordinates, surname = :plant_common_name WHERE name = :plant_name", database_name);

    client.unwrap().exec_drop(update_query, params! {
        "plant_coordinates" => plant_coordinates,
        "plant_common_name" => plant_common_name,
        "plant_name" => plant_name
    })
}

//--------------------------------------------------------------------------------
// fn remove_plant_from_field_database() {}
//
//--------------------------------------------------------------------------------
pub fn remove_plant_from_field_database(database_name: &String, plant_name: &String) -> std::result::Result<(), mysql::Error> {
    let client = connect_to_db_vanaprincipia();
    let remove_query = format!("DELETE FROM {} WHERE name = :plant_name", database_name);

    client.unwrap().exec_drop(remove_query, params! {
        "plant_name" => plant_name
    })
}

//--------------------------------------------------------------------------------
// fn add_plant_to_field_database() {}
//
//--------------------------------------------------------------------------------
pub fn add_plant_to_field_database(database_name: &String, plant_name: &String, plant_coordinates: &String) -> std::result::Result<(), mysql::Error> {
    let client = connect_to_db_vanaprincipia();
    let add_query = format!("INSERT INTO {} (name, coordinates) VALUES (:plant_name,:plant_coordinates)", database_name);

    client.unwrap().exec_drop(&add_query, params! {
        "plant_name" => plant_name, 
        "plant_coordinates" => plant_coordinates
    })
}

//--------------------------------------------------------------------------------
// fn get_field_plants() {}
//
// Get the data from the field_name table in the postgreSQL database
//
// cf. "hortipedia" is the name of the main table of the database, containing
// more than 32,000 plants parameters
//--------------------------------------------------------------------------------
pub fn get_field_plants(field_name: &String) -> Result<main_structures::PlantsByLifeformWithCoordinates> {

    let mut plants: Vec<main_structures::Plant> = Vec::new();

    let client = connect_to_db_vanaprincipia();

    //--------------------------------------------------------------------------------
    // SQL QUERY
    //--------------------------------------------------------------------------------
    let main_table = "hortipediadata";
    let first_inner_join = format!("INNER JOIN {} ON {}.name = hortipediadata.name", field_name, field_name);
    let ordering = format!("ORDER BY {}.name", field_name);
    let sqlquery = format!("SELECT {}.name FROM {} {} {};", field_name, main_table, first_inner_join, ordering);
    //--------------------------------------------------------------------------------

    println!("Executing the following SQL query: {}", sqlquery);

    let sqlresult = client.unwrap().query_map(
        sqlquery, 
        |plant_name| {
            let found_shape: main_structures::Shape = get_shape(&plant_name);
            let found_environment: main_structures::Environment = get_environment(&plant_name);

            let found_plant = main_structures::Plant{
                name: plant_name,
                shape: found_shape,
                environment: found_environment
            };

            plants.push(found_plant);
        }
    );

    if sqlresult.err().is_some() {
        println!("Error during SQL request");
    }
    
    let plants_by_lifeform = tools::parse_plants_by_lifeform(plants);
    let plants_by_lifeform_with_coordinates = tools::add_coordinates_from_field_to_plants(plants_by_lifeform, field_name);

    Ok(plants_by_lifeform_with_coordinates)
}

//--------------------------------------------------------------------------------
// fn get_plants_from_environment() {}
//
// Get the data from the field_name table in the postgreSQL database
//
// cf. "hortipedia" is the name of the main table of the database, containing
// more than 32,000 plants parameters
//--------------------------------------------------------------------------------
pub fn get_plants_from_environment(field_environment: Environment) -> Result<Vec<main_structures::Plant>> {

    let mut field_plants: Vec<main_structures::Plant> = Vec::new();

    let client = connect_to_db_vanaprincipia();

    //--------------------------------------------------------------------------------
    // SQL QUERY
    //--------------------------------------------------------------------------------
    let main_table = "hortipediadata";
    //let inner_join = format!("INNER JOIN edibleplants ON edibleplants.name = {}.name", main_table);

    let mut hardiness_range = "'10'".to_owned();
    let mut next_lowest_hardiness = 9;
    while next_lowest_hardiness >= field_environment.hardiness  {
        let value_to_add = format!(",'{}'", next_lowest_hardiness).to_owned();
        hardiness_range.push_str(&value_to_add);
        next_lowest_hardiness -= 1;
    }

    let environmental_condition = format!("WHERE exposure = '{}' AND moisture = '{}' AND soil = '{}' AND hardiness in ({})", field_environment.exposure, field_environment.moisture, field_environment.soil, hardiness_range);
    
    // If edible filter ON
    //let sqlquery = format!("SELECT {}.name FROM {} {} {};",main_table, main_table, inner_join, environmental_condition);
    
    let sqlquery = format!("SELECT {}.name FROM {} {};",main_table, main_table, environmental_condition);
    //--------------------------------------------------------------------------------

    println!("Get plants from specific environment. Query is: {}", sqlquery);

    let sqlresult = client.unwrap().query_map(
        sqlquery, 
        |plant_name| {
            let found_shape: main_structures::Shape = get_shape(&plant_name);
            let found_environment: main_structures::Environment = get_environment(&plant_name);

            let found_plant = main_structures::Plant{
                name: plant_name,
                shape: found_shape,
                environment: found_environment
            };

            field_plants.push(found_plant);
        }
    );

    if sqlresult.err().is_some() {
        println!("Error during SQL request");
    }

    Ok(field_plants)
}

//--------------------------------------------------------------------------------
// fn get_coordinates() {}
//
// Get coordinates in a usable format from the field_name table
//--------------------------------------------------------------------------------
pub fn get_coordinates(field_name: &String, plant_name: &String) -> Vec<(i32, i32)> {

    let mut coordinates_vector: Vec<(i32, i32)> = Vec::new();

    let client = connect_to_db_vanaprincipia();
    let sqlquery = format!("SELECT coordinates FROM {} WHERE name = '{}';", field_name, plant_name);

    let sqlresult = client.unwrap().query_map(
        sqlquery, 
        |coordinates| {
            println!("On {}, {} found at {}", field_name, plant_name, coordinates);
            coordinates_vector = tools::coordinates_converter(coordinates);
        }
    );

    if sqlresult.err().is_some() {
        println!("Error during SQL request");
    }
    
    coordinates_vector
}

//--------------------------------------------------------------------------------
// fn get_shape() {}
//
//  Get plant's shape parameters from the hortipedia table:
//      - lifeform
//      - habitus
//      - height
//      - leaf_shape
//      - leaf_division
//      - leaf_arrangement
//      - autumn_colour
//      - flower_colour
//      - flower_shape
//--------------------------------------------------------------------------------
pub fn get_shape(plant_name: &String) -> main_structures::Shape {

    let mut plant_shape: main_structures::Shape = main_structures::Shape{
        lifeform: " ".to_string(), 
        habitus: " ".to_string(), 
        height: " ".to_string(), 
        leaf_shape: " ".to_string(), 
        leaf_division: " ".to_string(), 
        leaf_arrangement: " ".to_string(), 
        autumn_colour: " ".to_string(), 
        flower_colour: " ".to_string(), 
        flower_shape: " ".to_string()};

    let client = connect_to_db_vanaprincipia();
    let sqlquery = format!("select lifeform, habitus, height, leafShape, leafDivision, leafArrangement, autumnColour, flowerColour, flowerShape from hortipediadata where name = '{}';", plant_name);

    //A lot of unwrap()... to be fixed? Rust noob is working here.
    let sqlresult = client.unwrap().query_map(
        sqlquery, 
        |(lifeform, habitus, height, leaf_shape, leaf_division, leaf_arrangement, autumn_colour, flower_colour, flower_shape)| {
            plant_shape.lifeform = lifeform;
            plant_shape.habitus = habitus;
            plant_shape.height = height;
            plant_shape.leaf_shape = leaf_shape;
            plant_shape.leaf_division = leaf_division;
            plant_shape.leaf_arrangement = leaf_arrangement;
            plant_shape.autumn_colour = autumn_colour;
            plant_shape.flower_colour = flower_colour;
            plant_shape.flower_shape = flower_shape;
        }
    );

    if sqlresult.err().is_some() {
        println!("Error during SQL request");
    }

    //println!("{} shape is: lifeform={} ; habitus={} ; height={} ; leaf_shape={} ; leaf_division={} ; leaf_arrangement={} ; autumn_colour={} ; flower_colour={} ; flower_shape={}", plant_name, plant_shape.lifeform, plant_shape.habitus, plant_shape.height, plant_shape.leaf_shape, plant_shape.leaf_division, plant_shape.leaf_arrangement, plant_shape.autumn_colour, plant_shape.flower_colour, plant_shape.flower_shape);

    plant_shape
}

//--------------------------------------------------------------------------------
// fn get_environment() {}
//
//  Get plant's environment parameters from the hortipedia table:
//      - exposure
//      - moisture
//      - soil
//      - hardiness
//--------------------------------------------------------------------------------
pub fn get_environment(plant_name: &String) -> main_structures::Environment {

    let mut plant_environment: main_structures::Environment = main_structures::Environment{exposure: 0, moisture: 0, soil: 0, hardiness: 0};

    let client = connect_to_db_vanaprincipia();
    let sqlquery = format!("select exposure, moisture, soil, hardiness from hortipediadata where name = '{}';", plant_name);

    let sqlresult = client.unwrap().query_map::<(String,String,String,String),_,_,_>(
        sqlquery, 
        |(exposure, moisture, soil, hardiness)| {
            if exposure != "_NA" {
                plant_environment.exposure = exposure.parse::<i32>().unwrap();
            }
            else {
                plant_environment.exposure = 0;
            }

            if moisture != "_NA" {
                plant_environment.moisture = moisture.parse::<i32>().unwrap();
            }
            else {
                plant_environment.moisture = 0;
            }

            if soil != "_NA" {
                plant_environment.soil = soil.parse::<i32>().unwrap();
            }
            else {
                plant_environment.soil = 0;
            }
    
            if hardiness != "_NA" {
                plant_environment.hardiness = hardiness.parse::<i32>().unwrap();
            }
            else {
                plant_environment.hardiness = 0;
            }
        }
    );

    if sqlresult.err().is_some() {
        println!("Error during SQL request");
    }
    
    //println!("{} environment is: exposure={} ; moisture={} ; soil={} ; hardiness={}", plant_name, plant_environment.exposure, plant_environment.moisture, plant_environment.soil, plant_environment.hardiness);

    plant_environment
}