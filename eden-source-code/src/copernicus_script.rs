extern crate execute;

use std::process::Command;

pub fn run_copernicus_script() {

     //Script to run
     let script_name = "eo_copernicus_script.sh";

     //ID
     let user = "antony.desmeaux";
     let pwd = "VanaPrincipia";
 
     //Configuration
     let coordinates = "4.484911,48.409947:4.545078,48.377347"; //Celeste field coordinates
     let product = "S2MSI2A";
     let hours_from_now = "240";
     /*
     Available values for the "product" parameter
     
     Sentinel-1: SLC, GRD, OCN
     Sentinel-2: S2MSI2A, S2MSI1C, S2MS2Ap
     Sentinel-3: SR_1_SRA___, SR_1_SRA_A, SR_1_SRA_BS, SR_2_LAN___,
                 OL_1_EFR___, OL_1_ERR___, OL_2_LFR___, OL_2_LRR___,
                 SL_1_RBT___, SL_2_LST___, SY_2_SYN___, SY_2_V10___,
                 SY_2_VG1___, SY_2_VGP___, SY_2_AOD__, SL_2_FRP__.
     Sentinel-5P: L1B_IR_SIR, L1B_IR_UVN, L1B_RA_BD1, L1B_RA_BD2,
                     L1B_RA_BD3, L1B_RA_BD4, L1B_RA_BD5, L1B_RA_BD6,
                     L1B_RA_BD7, L1B_RA_BD8, L2__AER_AI, L2__AER_LH,
                     L2__CH4, L2__CLOUD_, L2__CO____, L2__HCHO__,
                     L2__NO2___, L2__NP_BD3, L2__NP_BD6, L2__NP_BD7,
                     L2__O3_TCL, L2__O3____, L2__SO2___, AUX_CTMFCT,
                     AUX_CTMANA. 
     */
 
     //Command
     let command_to_run = format!("./../{}", script_name);

     //Arguments
     let arguments_to_pass = format!("-u {} -p {} -c {} -T {} -t {} -o product", user, pwd, coordinates, product, hours_from_now);
 
     print!("The command is {} {}\n", command_to_run, arguments_to_pass);
 
     Command::new(command_to_run)
             .arg("-u")
             .arg(user)
             .arg("-p")
             .arg(pwd)
             .arg("-c")
             .arg(coordinates)
             .arg("-T")
             .arg(product)
             .arg("-t")
             .arg(hours_from_now)
             .arg("-O copernicus_results")
             .spawn()
             .expect("failed to execute process");

}