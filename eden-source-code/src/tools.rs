//--------------------------------------------------------------------------------
// Used modules contained in the crate
//--------------------------------------------------------------------------------
use crate::main_structures::*;
use crate::database_actions;

extern crate rand;
use std::fs;
use rand::{thread_rng, Rng};

//--------------------------------------------------------------------------------
// fn coordinates_converter() {}
//
// This function allow, for each "(x;y)" in String like: "(4;4) (2;8) (2;9) ..."
// to push coordinates (x;y) inside a vector and return it at the end
//--------------------------------------------------------------------------------
pub fn coordinates_converter(coordinates: String) -> Vec<(i32, i32)> {
    
    let mut coordinates_vector: Vec<(i32, i32)> = Vec::new(); 

    let string_vec: Vec<&str> = coordinates.split_whitespace().collect();

    if string_vec[0] != "global" {
        for content in &string_vec {
            let coordinate_temp: Vec<&str> = content.split(&['(', ';', ')'][..]).collect();
    
            if !coordinate_temp.is_empty() {
                let x: i32 = coordinate_temp[1].parse::<i32>().unwrap();
                let y: i32 = coordinate_temp[2].parse::<i32>().unwrap();
        
                coordinates_vector.push((x,y));
            }
        }
    }

    coordinates_vector
}

//--------------------------------------------------------------------------------
// fn clear_coordinates_vectors() {}
//
//--------------------------------------------------------------------------------
pub fn clear_coordinates_vectors(adapted_plants_by_lifeform_with_coordinates: &mut PlantsByLifeformWithCoordinates, lifeform_coordinates: &mut LifeformCoordinates) {
    for tree in &mut adapted_plants_by_lifeform_with_coordinates.trees {
            tree.coordinates.clear();
    }
    for subshurb in &mut adapted_plants_by_lifeform_with_coordinates.subshurbs {
            subshurb.coordinates.clear();
    }
    for shurb in &mut adapted_plants_by_lifeform_with_coordinates.shurbs {
            shurb.coordinates.clear();
    }
    for climber in &mut adapted_plants_by_lifeform_with_coordinates.climbers {
            climber.coordinates.clear();
    }
    for perennial in &mut adapted_plants_by_lifeform_with_coordinates.perennials {
            perennial.coordinates.clear();
    }
    for annual_biennal in &mut adapted_plants_by_lifeform_with_coordinates.annual_biennals {
            annual_biennal.coordinates.clear();
    }
    for bulb in &mut adapted_plants_by_lifeform_with_coordinates.bulbs {
            bulb.coordinates.clear();
    }
    for grass in &mut adapted_plants_by_lifeform_with_coordinates.grass {
            grass.coordinates.clear();
    }

    lifeform_coordinates.trees_coordinates.clear();
    lifeform_coordinates.subshurbs_coordinates.clear();
    lifeform_coordinates.shurbs_coordinates.clear();
    lifeform_coordinates.climbers_coordinates.clear();
    lifeform_coordinates.perennials_coordinates.clear();
    lifeform_coordinates.annual_biennals_coordinates.clear();
    lifeform_coordinates.bulbs_coordinates.clear();
    lifeform_coordinates.grass_coordinates.clear();
}

//--------------------------------------------------------------------------------
// fn get_plants_from_random_environment() {}
//
// Set a random environment until a set of plants is found in the database
//--------------------------------------------------------------------------------
pub fn get_plants_from_random_environment() -> Vec<Plant> {

    let mut rng = thread_rng();

    let mut plants_from_environment = Vec::<Plant>::new();

    let mut field_environment: Environment;

    let vec_exposure = [1,12,123,2,23,3];
    let vec_moisture = [1,12,13,14,15,2,23,24,25,3,34,35,4,45,5];
    let vec_soil = [1, 12, 123, 1234, 2, 23, 234, 3, 34, 4];


    // Mainly for testing, less than 25 plants aren't very useful 
    while plants_from_environment.len() < 25 {

        field_environment = Environment{
            exposure: vec_exposure[rng.gen_range(0..=5)], 
            moisture: vec_moisture[rng.gen_range(0..=14)], 
            soil: vec_soil[rng.gen_range(0..=9)], 
            hardiness: rng.gen_range(1..=10)
        };

        plants_from_environment = database_actions::get_plants_from_environment(field_environment).unwrap();
    }

    println!("Number of different plants found = {}", plants_from_environment.len());

    plants_from_environment
}

//--------------------------------------------------------------------------------
// fn get_plants_from_environment() {}
//
// Get plants from user's environment parameters
//--------------------------------------------------------------------------------
pub fn get_plants_from_environment(user_environment: Environment) -> Vec<Plant> {

    let plants_from_environment = database_actions::get_plants_from_environment(user_environment).unwrap();

    println!("Number of different plants found = {}", plants_from_environment.len());

    plants_from_environment
}

//--------------------------------------------------------------------------------
// fn extract_existing_trees_coordinates() {}
//
// 
//--------------------------------------------------------------------------------
pub fn extract_existing_trees_coordinates(field_plants: &PlantsByLifeformWithCoordinates) -> Vec<(i32, i32)> {
    let mut vec_existing_trees = Vec::<(i32, i32)>::new();

    for tree in &field_plants.trees {
        for coordinate in &tree.coordinates {
            vec_existing_trees.push(*coordinate);
        }
    }

    vec_existing_trees
}

//--------------------------------------------------------------------------------
// fn extract_existing_subshurbs_coordinates() {}
//
// 
//--------------------------------------------------------------------------------
pub fn extract_existing_subshurbs_coordinates(field_plants: &PlantsByLifeformWithCoordinates) -> Vec<(i32, i32)> {
    let mut vec_existing_subshurbs = Vec::<(i32, i32)>::new();

    for tree in &field_plants.subshurbs {
        for coordinate in &tree.coordinates {
            vec_existing_subshurbs.push(*coordinate);
        }
    }

    vec_existing_subshurbs
}

//--------------------------------------------------------------------------------
// fn extract_existing_shurbs_coordinates() {}
//
// 
//--------------------------------------------------------------------------------
pub fn extract_existing_shurbs_coordinates(field_plants: &PlantsByLifeformWithCoordinates) -> Vec<(i32, i32)> {
    let mut vec_existing_shurbs = Vec::<(i32, i32)>::new();

    for tree in &field_plants.shurbs {
        for coordinate in &tree.coordinates {
            vec_existing_shurbs.push(*coordinate);
        }
    }

    vec_existing_shurbs
}

//--------------------------------------------------------------------------------
// fn extract_existing_climbers_coordinates() {}
//
// 
//--------------------------------------------------------------------------------
pub fn extract_existing_climbers_coordinates(field_plants: &PlantsByLifeformWithCoordinates) -> Vec<(i32, i32)> {
    let mut vec_existing_climbers = Vec::<(i32, i32)>::new();

    for tree in &field_plants.climbers {
        for coordinate in &tree.coordinates {
            vec_existing_climbers.push(*coordinate);
        }
    }

    vec_existing_climbers
}

//--------------------------------------------------------------------------------
// fn extract_existing_perennials_coordinates() {}
//
// 
//--------------------------------------------------------------------------------
pub fn extract_existing_perennials_coordinates(field_plants: &PlantsByLifeformWithCoordinates) -> Vec<(i32, i32)> {
    let mut vec_existing_perennials = Vec::<(i32, i32)>::new();

    for tree in &field_plants.perennials {
        for coordinate in &tree.coordinates {
            vec_existing_perennials.push(*coordinate);
        }
    }

    vec_existing_perennials
}

//--------------------------------------------------------------------------------
// fn extract_existing_annual_biennals_coordinates() {}
//
// 
//--------------------------------------------------------------------------------
pub fn extract_existing_annual_biennals_coordinates(field_plants: &PlantsByLifeformWithCoordinates) -> Vec<(i32, i32)> {
    let mut vec_existing_annual_biennals = Vec::<(i32, i32)>::new();

    for tree in &field_plants.annual_biennals {
        for coordinate in &tree.coordinates {
            vec_existing_annual_biennals.push(*coordinate);
        }
    }

    vec_existing_annual_biennals
}

//--------------------------------------------------------------------------------
// fn extract_existing_bulbs_coordinates() {}
//
// 
//--------------------------------------------------------------------------------
pub fn extract_existing_bulbs_coordinates(field_plants: &PlantsByLifeformWithCoordinates) -> Vec<(i32, i32)> {
    let mut vec_existing_bulbs = Vec::<(i32, i32)>::new();

    for tree in &field_plants.bulbs {
        for coordinate in &tree.coordinates {
            vec_existing_bulbs.push(*coordinate);
        }
    }

    vec_existing_bulbs
}

//--------------------------------------------------------------------------------
// fn extract_existing_grass_coordinates() {}
//
// 
//--------------------------------------------------------------------------------
pub fn extract_existing_grass_coordinates(field_plants: &PlantsByLifeformWithCoordinates) -> Vec<(i32, i32)> {
    let mut vec_existing_grass = Vec::<(i32, i32)>::new();

    for tree in &field_plants.grass {
        for coordinate in &tree.coordinates {
            vec_existing_grass.push(*coordinate);
        }
    }

    vec_existing_grass
}

//--------------------------------------------------------------------------------
// fn extract_existing_coordinates() {}
//
// 
//--------------------------------------------------------------------------------
pub fn extract_existing_plants_coordinates(field_plants: &PlantsByLifeformWithCoordinates) -> Vec<(i32, i32)> {
    let mut vec_existing_plants = Vec::<(i32, i32)>::new();

    for tree in &field_plants.trees {
        for coordinate in &tree.coordinates {
            vec_existing_plants.push(*coordinate);
        }
    }

    for subshurb in &field_plants.subshurbs {
        for coordinate in &subshurb.coordinates {
            vec_existing_plants.push(*coordinate);
        }
    }

    for shurb in &field_plants.shurbs {
        for coordinate in &shurb.coordinates {
            vec_existing_plants.push(*coordinate);
        }
    }

    for climber in &field_plants.climbers {
        for coordinate in &climber.coordinates {
            vec_existing_plants.push(*coordinate);
        }
    }

    for perennial in &field_plants.perennials {
        for coordinate in &perennial.coordinates {
            vec_existing_plants.push(*coordinate);
        }
    }

    for annual_biennal in &field_plants.annual_biennals {
        for coordinate in &annual_biennal.coordinates {
            vec_existing_plants.push(*coordinate);
        }
    }

    for bulb in &field_plants.bulbs {
        for coordinate in &bulb.coordinates {
            vec_existing_plants.push(*coordinate);
        }
    }

    for grass in &field_plants.grass {
        for coordinate in &grass.coordinates {
            vec_existing_plants.push(*coordinate);
        }
    }

    for path_coordinate in &field_plants.path {
        vec_existing_plants.push(*path_coordinate);
    }

    vec_existing_plants
}

//--------------------------------------------------------------------------------
// fn parse_plants_by_lifeform() {}
//
// Parse plants by lifeform: trees, subshurbs, shurbs, climbers, perennials,
// annual_biennals, bulbs or grass
//--------------------------------------------------------------------------------
pub fn parse_plants_by_lifeform(plants: Vec<Plant>) -> PlantsByLifeform {

    let mut plants_by_lifeform = PlantsByLifeform {
        trees: Vec::<Plant>::new(),
        subshurbs: Vec::<Plant>::new(),
        shurbs: Vec::<Plant>::new(),
        climbers: Vec::<Plant>::new(),
        perennials: Vec::<Plant>::new(),
        annual_biennals: Vec::<Plant>::new(),
        bulbs: Vec::<Plant>::new(),
        grass: Vec::<Plant>::new()
    };

    for plant in plants {
        if plant.shape.lifeform == "1" {
            plants_by_lifeform.trees.push(plant);
        }
        else if plant.shape.lifeform == "2" {
            plants_by_lifeform.subshurbs.push(plant);
        }
        else if plant.shape.lifeform == "3" {
            plants_by_lifeform.shurbs.push(plant);
        }
        else if plant.shape.lifeform == "4" {
            plants_by_lifeform.climbers.push(plant);
        }
        else if plant.shape.lifeform == "5" {
            plants_by_lifeform.perennials.push(plant);
        }
        else if plant.shape.lifeform == "6" {
            plants_by_lifeform.annual_biennals.push(plant);
        }
        else if plant.shape.lifeform == "7" {
            plants_by_lifeform.bulbs.push(plant);
        }
        else if plant.shape.lifeform == "8" {
            plants_by_lifeform.grass.push(plant);
        }
        else {
            println!("Unidentified lifeform during field_designer::order_plants_by_lifeform()");
        }
    }

    plants_by_lifeform
}

//--------------------------------------------------------------------------------
// fn generate_available_coordinates() {}
//
// Create a vector of available coordinates regarding field's size
//--------------------------------------------------------------------------------
pub fn generate_available_coordinates(field_size: &FieldSize) -> Vec::<(i32, i32)> {

    let mut available_coordinates = Vec::<(i32, i32)>::new();

    let mut x_axis = 0;
    let mut y_axis = 0;

    while y_axis < field_size.width {
        while x_axis < field_size.length {
            available_coordinates.push((x_axis, y_axis));
            x_axis += 1;
            //available_coordinates = (0;0),(1;0),(2;0),...(length;width)
        }
        y_axis += 1;
        x_axis = 0;
    }

    available_coordinates
}

//--------------------------------------------------------------------------------
// fn remove_input_coordinates() {}
//
// Remove coordinates from available coordinates (path, existing plants, etc.)
//--------------------------------------------------------------------------------
pub fn remove_input_coordinates(available_coordinates: &mut Vec::<(i32, i32)>, coordinates_to_be_removed: &Vec::<(i32, i32)>) {

    for coordinate in coordinates_to_be_removed {
        let index = available_coordinates.iter().position(|x| *x == *coordinate);
        if index != None {
            available_coordinates.remove(index.unwrap());
        }
    }
}

//--------------------------------------------------------------------------------
// fn set_random_path_coordinates() {}
//
//
//--------------------------------------------------------------------------------
pub fn get_random_path_coordinates(field_size: &FieldSize) -> Vec::<(i32,i32)> {

    let mut path_coordinates = Vec::<(i32, i32)>::new();

    let last_range_width = 5;
    let mut x_value = 0;

    let one_third_width = field_size.width / 3;
    println!("One third of the field = {} meters", one_third_width);

    let mut y_value_beginning = one_third_width;
    let y_value_end = y_value_beginning * 2;

    /*
    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    */

    while x_value < field_size.length - last_range_width {
        path_coordinates.push((x_value, y_value_beginning));
        x_value += 1;
    }

    /*
    OOOOOOOOOOOOOOOOOOOOOOOOOOOOO
                                X
                                X
                                X
                                X
                                X
    */
    x_value -= 1; //go back to last valid x_value

    while y_value_beginning < y_value_end {
        y_value_beginning += 1;
        path_coordinates.push((x_value, y_value_beginning));
    }

    /*
    OOOOOOOOOOOOOOOOOOOOOOOOOOOOO
                                O
                                O
                                O
                                O
                                O
    XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    */
    while x_value >= 0 {
        path_coordinates.push((x_value, y_value_end));
        x_value -= 1;
    }

    path_coordinates
}

//--------------------------------------------------------------------------------
// fn add_coordinates_to_plants() {}
//
// 
//--------------------------------------------------------------------------------
pub fn add_coordinates_from_field_to_plants(plants_by_lifeform: PlantsByLifeform, field_name: &String) -> PlantsByLifeformWithCoordinates {

    let mut field_plants_by_lifeform = PlantsByLifeformWithCoordinates {
        trees: Vec::<PlantWithCoordinates>::new(),
        subshurbs: Vec::<PlantWithCoordinates>::new(),
        shurbs: Vec::<PlantWithCoordinates>::new(),
        climbers: Vec::<PlantWithCoordinates>::new(),
        perennials: Vec::<PlantWithCoordinates>::new(),
        annual_biennals: Vec::<PlantWithCoordinates>::new(),
        bulbs: Vec::<PlantWithCoordinates>::new(),
        grass: Vec::<PlantWithCoordinates>::new(),
        path: Vec::<(i32,i32)>::new()
    };

    for tree in plants_by_lifeform.trees {
        let tree_with_coordinates = PlantWithCoordinates {
            name: tree.name.clone(), 
            shape: tree.shape.clone(), 
            environment: tree.environment, 
            coordinates: database_actions::get_coordinates(field_name, &tree.name)
        };
        field_plants_by_lifeform.trees.push(tree_with_coordinates);
    }

    for subshurb in plants_by_lifeform.subshurbs {
        let subshurb_with_coordinates = PlantWithCoordinates {
            name: subshurb.name.clone(), 
            shape: subshurb.shape.clone(), 
            environment: subshurb.environment, 
            coordinates: database_actions::get_coordinates(field_name, &subshurb.name)
        };
        field_plants_by_lifeform.subshurbs.push(subshurb_with_coordinates);
    }

    for shurb in plants_by_lifeform.shurbs {
        let shurb_with_coordinates = PlantWithCoordinates {
            name: shurb.name.clone(), 
            shape: shurb.shape.clone(), 
            environment: shurb.environment, 
            coordinates: database_actions::get_coordinates(field_name, &shurb.name)
        };
        field_plants_by_lifeform.shurbs.push(shurb_with_coordinates);
    }

    for climber in plants_by_lifeform.climbers {
        let climber_with_coordinates = PlantWithCoordinates {
            name: climber.name.clone(), 
            shape: climber.shape.clone(), 
            environment: climber.environment, 
            coordinates: database_actions::get_coordinates(field_name, &climber.name)
        };
        field_plants_by_lifeform.climbers.push(climber_with_coordinates);
    }

    for perennial in plants_by_lifeform.perennials {
        let perennial_with_coordinates = PlantWithCoordinates {
            name: perennial.name.clone(), 
            shape: perennial.shape.clone(), 
            environment: perennial.environment, 
            coordinates: database_actions::get_coordinates(field_name, &perennial.name)
        };
        field_plants_by_lifeform.perennials.push(perennial_with_coordinates);
    }

    for annual_biennal in plants_by_lifeform.annual_biennals {
        let annual_biennal_with_coordinates = PlantWithCoordinates {
            name: annual_biennal.name.clone(), 
            shape: annual_biennal.shape.clone(), 
            environment: annual_biennal.environment, 
            coordinates: database_actions::get_coordinates(field_name, &annual_biennal.name)
        };
        field_plants_by_lifeform.annual_biennals.push(annual_biennal_with_coordinates);
    }

    for bulb in plants_by_lifeform.bulbs {
        let bulb_with_coordinates = PlantWithCoordinates {
            name: bulb.name.clone(), 
            shape: bulb.shape.clone(), 
            environment: bulb.environment, 
            coordinates: database_actions::get_coordinates(field_name, &bulb.name)
        };
        field_plants_by_lifeform.bulbs.push(bulb_with_coordinates);
    }

    for grass in plants_by_lifeform.grass {
        let grass_with_coordinates = PlantWithCoordinates {
            name: grass.name.clone(), 
            shape: grass.shape.clone(), 
            environment: grass.environment, 
            coordinates: database_actions::get_coordinates(field_name, &grass.name)
        };
        field_plants_by_lifeform.grass.push(grass_with_coordinates);
    }

    field_plants_by_lifeform
}

//--------------------------------------------------------------------------------
// fn add_empty_coordinate_parameter_to_plants() {}
//
// 
//--------------------------------------------------------------------------------
pub fn add_empty_coordinate_parameter_to_plants(plants_by_lifeform: &PlantsByLifeform) -> PlantsByLifeformWithCoordinates {

    let mut field_plants_by_lifeform = PlantsByLifeformWithCoordinates {
        trees: Vec::<PlantWithCoordinates>::new(),
        subshurbs: Vec::<PlantWithCoordinates>::new(),
        shurbs: Vec::<PlantWithCoordinates>::new(),
        climbers: Vec::<PlantWithCoordinates>::new(),
        perennials: Vec::<PlantWithCoordinates>::new(),
        annual_biennals: Vec::<PlantWithCoordinates>::new(),
        bulbs: Vec::<PlantWithCoordinates>::new(),
        grass: Vec::<PlantWithCoordinates>::new(),
        path: Vec::<(i32,i32)>::new()
    };

    for tree in &plants_by_lifeform.trees {
        let tree_with_coordinates = PlantWithCoordinates {
            name: tree.name.clone(), 
            shape: tree.shape.clone(), 
            environment: tree.environment, 
            coordinates: Vec::<(i32, i32)>::new()
        };
        field_plants_by_lifeform.trees.push(tree_with_coordinates);
    }

    for subshurb in &plants_by_lifeform.subshurbs {
        let subshurb_with_coordinates = PlantWithCoordinates {
            name: subshurb.name.clone(), 
            shape: subshurb.shape.clone(), 
            environment: subshurb.environment, 
            coordinates: Vec::<(i32, i32)>::new()
        };
        field_plants_by_lifeform.subshurbs.push(subshurb_with_coordinates);
    }

    for shurb in &plants_by_lifeform.shurbs {
        let shurb_with_coordinates = PlantWithCoordinates {
            name: shurb.name.clone(), 
            shape: shurb.shape.clone(), 
            environment: shurb.environment, 
            coordinates: Vec::<(i32, i32)>::new()
        };
        field_plants_by_lifeform.shurbs.push(shurb_with_coordinates);
    }

    for climber in &plants_by_lifeform.climbers {
        let climber_with_coordinates = PlantWithCoordinates {
            name: climber.name.clone(), 
            shape: climber.shape.clone(), 
            environment: climber.environment, 
            coordinates: Vec::<(i32, i32)>::new()
        };
        field_plants_by_lifeform.climbers.push(climber_with_coordinates);
    }

    for perennial in &plants_by_lifeform.perennials {
        let perennial_with_coordinates = PlantWithCoordinates {
            name: perennial.name.clone(), 
            shape: perennial.shape.clone(), 
            environment: perennial.environment, 
            coordinates: Vec::<(i32, i32)>::new()
        };
        field_plants_by_lifeform.perennials.push(perennial_with_coordinates);
    }

    for annual_biennal in &plants_by_lifeform.annual_biennals {
        let annual_biennal_with_coordinates = PlantWithCoordinates {
            name: annual_biennal.name.clone(), 
            shape: annual_biennal.shape.clone(), 
            environment: annual_biennal.environment, 
            coordinates: Vec::<(i32, i32)>::new()
        };
        field_plants_by_lifeform.annual_biennals.push(annual_biennal_with_coordinates);
    }

    for bulb in &plants_by_lifeform.bulbs {
        let bulb_with_coordinates = PlantWithCoordinates {
            name: bulb.name.clone(), 
            shape: bulb.shape.clone(), 
            environment: bulb.environment, 
            coordinates: Vec::<(i32, i32)>::new()
        };
        field_plants_by_lifeform.bulbs.push(bulb_with_coordinates);
    }

    for grass in &plants_by_lifeform.grass {
        let grass_with_coordinates = PlantWithCoordinates {
            name: grass.name.clone(), 
            shape: grass.shape.clone(), 
            environment: grass.environment, 
            coordinates: Vec::<(i32, i32)>::new()
        };
        field_plants_by_lifeform.grass.push(grass_with_coordinates);
    }

    field_plants_by_lifeform
}

//--------------------------------------------------------------------------------
// fn environment_parameters_translator() {}
//
// Translate environment parameters to its human readable value
//--------------------------------------------------------------------------------
pub fn environment_parameters_translator(environment_parameters: Environment) -> EnvironmentTranslated {
    let exposure_str: &str;
    if environment_parameters.exposure == 1 {
        exposure_str = "Full sun";
    }
    else if environment_parameters.exposure == 12 {
        exposure_str = "Full sun to half shade";
    }
    else if environment_parameters.exposure == 13 {
        exposure_str = "Full sun to full shade";
    }
    else if environment_parameters.exposure == 2 {
        exposure_str = "Half shade";
    }
    else if environment_parameters.exposure == 23 {
        exposure_str = "Half shade to full shade";
    }
    else if environment_parameters.exposure == 3 {
        exposure_str = "Full shade";
    }
    else {
        exposure_str = "N.A.";
    }

    let moisture_str: &str;
    if environment_parameters.moisture == 1 {
        moisture_str = "Dry";
    }
    else if environment_parameters.moisture == 12 {
        moisture_str = "Dry to fresh";
    }
    else if environment_parameters.moisture == 13 {
        moisture_str = "Dry to moist";
    }
    else if environment_parameters.moisture == 14 {
        moisture_str = "Dry to wet";
    }
    else if environment_parameters.moisture == 15 {
        moisture_str = "Dry to aquatic";
    }
    else if environment_parameters.moisture == 2 {
        moisture_str = "Fresh";
    }
    else if environment_parameters.moisture == 23 {
        moisture_str = "Fresh to moist";
    }
    else if environment_parameters.moisture == 24 {
        moisture_str = "Fresh to wet";
    }
    else if environment_parameters.moisture == 25 {
        moisture_str = "Fresh to aquatic";
    }
    else if environment_parameters.moisture == 3 {
        moisture_str = "Moist";
    }
    else if environment_parameters.moisture == 34 {
        moisture_str = "Moist to wet";
    }
    else if environment_parameters.moisture == 35 {
        moisture_str = "Moist to aquatic";
    }
    else if environment_parameters.moisture == 4 {
        moisture_str = "Wet";
    }
    else if environment_parameters.moisture == 45 {
        moisture_str = "Wet to aquatic";
    }
    else if environment_parameters.moisture == 5 {
        moisture_str = "Aquatic";
    }
    else {
        moisture_str = "N.A.";
    }

    let soil_str: &str;
    if environment_parameters.soil == 1 {
        soil_str = "Light soil";
    }
    else if environment_parameters.soil == 2 {
        soil_str = "Medium soil";
    }
    else if environment_parameters.soil == 3 {
        soil_str = "Heavy soil";
    }
    else if environment_parameters.soil == 4 {
        soil_str = "Peaty soil";
    }
    else {
        soil_str = "N.A.";
    }

    let hardiness_str: &str;
    if environment_parameters.hardiness == 1 {
        hardiness_str = "Below -45°C";
    }
    else if environment_parameters.hardiness == 2 {
        hardiness_str = "Down to -45°C";
    }
    else if environment_parameters.hardiness == 3 {
        hardiness_str = "Down to -40°C";
    }
    else if environment_parameters.hardiness == 4 {
        hardiness_str = "Down to -35°C";
    }
    else if environment_parameters.hardiness == 5 {
        hardiness_str = "Down to -29°C";
    }
    else if environment_parameters.hardiness == 6 {
        hardiness_str = "Down to -23°C";
    }
    else if environment_parameters.hardiness == 7 {
        hardiness_str = "Down to -18°C";
    }
    else if environment_parameters.hardiness == 8 {
        hardiness_str = "Down to -12°C";
    }
    else if environment_parameters.hardiness == 9 {
        hardiness_str = "Down to -7°C";
    }
    else if environment_parameters.hardiness == 10 {
        hardiness_str = "At least 1°C";
    }
    else {
        hardiness_str = "N.A.";
    }

    let translated_environment = EnvironmentTranslated {
        exposure: exposure_str.to_string(),
        moisture: moisture_str.to_string(),
        soil: soil_str.to_string(),
        hardiness: hardiness_str.to_string()
    };

    translated_environment
}

//--------------------------------------------------------------------------------
// fn environment_parameters_translator() {}
//
// Translate environment parameters to its human readable value
//--------------------------------------------------------------------------------
pub fn save_field_plant_in_file(field_plant: &PlantsByLifeformWithCoordinates, field_plants_names: &Vec::<String>) {

    let mut data = String::new();
    let mut plant_number = 1;

    fn write_plant(field_plants_names: &Vec::<String>, plant: &PlantWithCoordinates, data: &mut String, plant_number: &mut i32, plant_type: &str) {
        let plant_name = plant.name.clone();
        let mut lifeform = plant_type.clone().to_string();
        if !plant.coordinates.is_empty() {
            for name in field_plants_names {
                if name == &plant_name {
                    lifeform = format!("EXISTING {}", plant_type);
                }
            }

            data.push_str(format!("#{} - {}: https://fr.hortipedia.com/{} \n", plant_number, lifeform, plant_name).as_str());
            data.push_str(format!("Coordinates are: {:?}\n\n", plant.coordinates).as_str());

            *plant_number += 1;
        }
    }


    for plant in &field_plant.trees {
        write_plant(&field_plants_names, &plant, &mut data, &mut plant_number, "Tree");
    }

    for plant in &field_plant.subshurbs {
        write_plant(&field_plants_names, &plant, &mut data, &mut plant_number, "Subshurb");
    }

    for plant in &field_plant.shurbs {
        write_plant(&field_plants_names, &plant, &mut data, &mut plant_number, "Shurb");
    }

    for plant in &field_plant.climbers {
        write_plant(&field_plants_names, &plant, &mut data, &mut plant_number, "Climber");
    }

    for plant in &field_plant.perennials {
        write_plant(&field_plants_names, &plant, &mut data, &mut plant_number, "Perennial");
    }

    for plant in &field_plant.annual_biennals {
        write_plant(&field_plants_names, &plant, &mut data, &mut plant_number, "Annual/Biennal");
    }

    for plant in &field_plant.bulbs {
        write_plant(&field_plants_names, &plant, &mut data, &mut plant_number, "Bulb");
    }

    for plant in &field_plant.grass {
        write_plant(&field_plants_names, &plant, &mut data, &mut plant_number, "Grass");
    }

    fs::write("results/field_plants.txt", data).expect("Unable to write file");
}

//--------------------------------------------------------------------------------
// fn extract_plants_name() {}
//
// Extract plants' name from a PlantsByLifeformWithCoordinates object
//--------------------------------------------------------------------------------
pub fn extract_plants_name(field_plants: &PlantsByLifeformWithCoordinates) -> Vec::<String> {
    let mut field_plants_names = Vec::<String>::new();
    for plant in &field_plants.trees {
            field_plants_names.push(plant.name.clone());
    }
    for plant in &field_plants.subshurbs {
            field_plants_names.push(plant.name.clone());
    }
    for plant in &field_plants.shurbs {
            field_plants_names.push(plant.name.clone());
    }
    for plant in &field_plants.climbers {
            field_plants_names.push(plant.name.clone());
    }
    for plant in &field_plants.perennials {
            field_plants_names.push(plant.name.clone());
    }
    for plant in &field_plants.annual_biennals {
            field_plants_names.push(plant.name.clone());
    }
    for plant in &field_plants.bulbs {
            field_plants_names.push(plant.name.clone());
    }
    for plant in &field_plants.grass {
            field_plants_names.push(plant.name.clone());
    }

    field_plants_names
}