#[derive(Clone)]
pub struct Shape {
    pub lifeform: String,
    pub habitus: String,
    pub height: String,
    pub leaf_shape: String,
    pub leaf_division: String,
    pub leaf_arrangement: String,
    pub autumn_colour: String,
    pub flower_colour: String,
    pub flower_shape: String
}

#[derive(Copy,Clone)]
pub struct Environment {
    pub exposure: i32,
    pub moisture: i32,
    pub soil: i32,
    pub hardiness: i32
}


pub struct EnvironmentTranslated {
    pub exposure: String,
    pub moisture: String,
    pub soil: String,
    pub hardiness: String
}

pub struct FieldPlants {
    pub plants: Vec<PlantWithCoordinates>
}

pub struct FieldSize {
    pub length: i32,
    pub width: i32
}

pub struct FieldPart {
    pub environment: Environment,
    pub coordinates: (i32, i32),
    pub size: FieldSize
    //pub shape: Square
}

pub struct DesignedField {
    pub field_parts: Vec<FieldPart>
}

pub struct Plant {
    pub name: String,
    pub shape: Shape,
    pub environment: Environment
}

pub struct PlantsByLifeform {
    pub trees: Vec::<Plant>,
    pub subshurbs: Vec::<Plant>,
    pub shurbs: Vec::<Plant>,
    pub climbers: Vec::<Plant>,
    pub perennials: Vec::<Plant>,
    pub annual_biennals: Vec::<Plant>,
    pub bulbs: Vec::<Plant>,
    pub grass: Vec::<Plant>
}

pub struct LifeformCoordinates {
    pub trees_coordinates: Vec::<(i32, i32)>,
    pub subshurbs_coordinates: Vec::<(i32, i32)>,
    pub shurbs_coordinates: Vec::<(i32, i32)>,
    pub climbers_coordinates: Vec::<(i32, i32)>,
    pub perennials_coordinates: Vec::<(i32, i32)>,
    pub annual_biennals_coordinates: Vec::<(i32, i32)>,
    pub bulbs_coordinates: Vec::<(i32, i32)>,
    pub grass_coordinates: Vec::<(i32, i32)>
}

#[derive(Clone)]
pub struct PlantWithCoordinates {
    pub name: String,
    pub shape: Shape,
    pub environment: Environment,
    pub coordinates: Vec<(i32, i32)>
}

pub struct PlantsByLifeformWithCoordinates {
    pub trees: Vec::<PlantWithCoordinates>,
    pub subshurbs: Vec::<PlantWithCoordinates>,
    pub shurbs: Vec::<PlantWithCoordinates>,
    pub climbers: Vec::<PlantWithCoordinates>,
    pub perennials: Vec::<PlantWithCoordinates>,
    pub annual_biennals: Vec::<PlantWithCoordinates>,
    pub bulbs: Vec::<PlantWithCoordinates>,
    pub grass: Vec::<PlantWithCoordinates>,
    pub path: Vec::<(i32,i32)>
}